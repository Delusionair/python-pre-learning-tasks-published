def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        res = int(a + b)
    elif operator == "-":
        res = int(a - b)
    elif operator == "*":
        res = int(a * b)
    elif operator == "/":
        res = int(a / b)
    else:
        print("Input valid operators: +, - , * , /")
    # ==============
    return bin(res)[2:]         #bin() function converts resulting value into binary and with [2:] at the end to remove any zeros which are before the first '1'
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
