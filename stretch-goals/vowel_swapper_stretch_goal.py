def vowel_swapper(string):
    # ==============
    # Your code here
    counter = 1
    vowels = "aeiouAEIOU"
    for i in range(0,len(string)):
        if string[i] in vowels:
            counter = counter + 1
            if counter % 3 == 0:
                if string[i] == "a":
                    string = string.replace('a','/\\')
                elif string[i] == "A":
                    string = string.replace('A','/\\')
                elif string[i] == "e":
                    string = string.replace('e','3')
                elif string[i] == "E":
                    string = string.replace('E','3')
                elif string[i] == "i":
                    string = string.replace('i','!')
                elif string[i] == "I":
                    string = string.replace('I','!')
                elif string[i] == "o":
                    string = string.replace('o','oo')
                elif string[i] == "O":
                    string = string.replace('O','000')
                elif string[i] == "u":
                    string = string.replace('u','\/')
                else:
                    string = string.replace('U','\/')
            
    return string 
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
